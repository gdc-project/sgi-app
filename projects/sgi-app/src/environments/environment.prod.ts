export const environment = {
  production: true,
  apipe: {
    url: 'https://dniruc.apisperu.com/api/v1',
    token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6InZjb3JkZXJvZXNwaW5vemFAZ21haWwuY29tIn0.jsa8EHaw1E_0ih_vHsZBWBeSRimClgYHNFj44N2ne2k',
  },
  firebase: {
    apiKey: 'AIzaSyDrogEJmqo_qjrfjfc683lZv_IMvwe5CBg',
    authDomain: 'gdc-project-dev.firebaseapp.com',
    databaseURL: 'https://gdc-project-dev.firebaseio.com',
    projectId: 'gdc-project-dev',
    storageBucket: 'gdc-project-dev.appspot.com',
    messagingSenderId: '149526792903',
    appId: '1:149526792903:web:164b0b3ee7865257b38ce2',
    measurementId: 'G-KMZP67MHG2'
  }
};
