import { UserService } from './../../core/security/user.service';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { User } from '../../core/models/user.model';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  user: User;
  user$ = this.usersService.user$;
  modalRef: BsModalRef;
  message: string;

  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    roles: new FormGroup({
      admin: new FormControl(false),
      coordinator: new FormControl(false),
      manager: new FormControl(false)
    }),
    isActive: new FormControl(false)
  });

  constructor(
    private usersService: UserService,
    private modalService: BsModalService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.usersService.getAll();
  }

  isValidControl(controlName: string) {
    return this.form.controls[controlName].valid || this.form.controls[controlName].untouched;
  }

  save() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.usersService.add(this.form.value).subscribe(
        () => {
          this.form.reset();
          this.toastr.success('El usuario fue añadido');
        },
        error => {
          this.form.controls.email.setErrors(error);
        }
      );
    }
  }

  toogleRole(user: User, role: string, value) {
    this.usersService.updateRole(user.email, role, value.target.checked);
  }

  toogleStatus(user: User, value) {
    this.usersService.updateStatus(user.email, value.target.checked);
  }

  openConfirmation(template: TemplateRef<any>, user: User) {
    this.user = user;
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  confirmDelete(): void {
    this.delete(this.user);
    this.modalRef.hide();
  }

  declineDelete(): void {
    this.modalRef.hide();
  }

  delete(user: User) {
    this.usersService.delete(user.email).then(
      () => this.toastr.success('El usuario fue eliminado')
    );
  }
}
