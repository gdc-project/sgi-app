export enum Collection {
  Incidents = 'incidents',
  Areas = 'areas',
  Districts = 'districts',
  Typifications = 'typifications',
  Citizens = 'citizens',
  Users = 'users'
}
