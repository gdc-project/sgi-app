export interface Roles {
  admin?: boolean;
  coordinator?: boolean;
  manager?: boolean;
}
