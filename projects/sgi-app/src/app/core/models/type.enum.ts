export enum IncidentType {
    Orientation = 'Orientación',
    Claim = 'Reclamo',
    Suggestion = 'Sugerencia'
}
