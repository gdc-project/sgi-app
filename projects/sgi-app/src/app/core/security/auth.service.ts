import { User } from './../models/user.model';
import { Collection } from './../models/collection.enum';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap, take, map, tap } from 'rxjs/operators';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AuthService {

  user: User;
  user$: Observable<User>;

  constructor(
    private fireAuth: AngularFireAuth,
    private firestore: AngularFirestore,
    private router: Router,
    private toastr: ToastrService
  ) {
    this.user$ = this.fireAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.firestore
            .collection(Collection.Users)
            .doc<User>(user.email)
            .valueChanges();
        } else {
          return of(null);
        }
      }),
      tap(user => this.user = user)
    );
  }

  private checkPermissions(user: User) {
    const allowed = this.firestore
      .collection(Collection.Users)
      .doc(user.email)
      .snapshotChanges()
      .pipe(take(1));

    allowed.subscribe(snapshot => {
      const isAllowed = snapshot.payload.data();
      if (isAllowed) {
        this.updateUserData(user);
        this.toastr.info(user.displayName, 'Bienvenido');
        this.router.navigateByUrl('/');
      } else {
        this.fireAuth.signOut();
      }
    });
  }

  async googleSignin() {
    const provider = new auth.GoogleAuthProvider();
    this.fireAuth.signInWithPopup(provider).then(
      response => this.checkPermissions(response.user));
  }

  private updateUserData(user) {
    const data: any = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL
    };

    this.firestore
      .collection(Collection.Users)
      .doc(user.email)
      .set(data, { merge: true });
  }

  async signOut() {
    this.router.navigateByUrl('/ingresar');
    await this.fireAuth.signOut();
  }
}
