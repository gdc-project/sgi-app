import { User } from './../models/user.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { map, take, tap } from 'rxjs/operators';
import { BehaviorSubject, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  user$ = new BehaviorSubject<User[]>([]);

  constructor(private firestore: AngularFirestore) { }

  getAll() {
    this.firestore
      .collection('users')
      .snapshotChanges()
      .pipe(
        map(res => res.map(item => item.payload.doc.data() as User)),
      ).subscribe(response => this.user$.next(response));
  }

  search(filter: string) {
    const found = this.firestore
      .collection('users', ref => ref.where('email', '==', filter));

    found.get().subscribe(ref => {
      if (ref.size) {
        found.snapshotChanges().pipe(
          map(res => res.map(item => item.payload.doc.data() as User)),
        ).subscribe(response => this.user$.next(response));
      } else {
        this.user$.next([]);
      }
    });
  }

  updateRole(email: string, role: string, value: boolean) {
    const roles = {};
    roles[role] = value;

    this.firestore
      .collection('users')
      .doc(email)
      .set({ roles }, { merge: true });
  }

  updateStatus(email: string, value: boolean) {
    this.firestore
      .collection('users')
      .doc(email)
      .set({ isActive: value }, { merge: true });
  }

  add(user: User) {
    const document = this.firestore
      .collection('users')
      .doc(user.email);

    return document.get().pipe(
      tap(snapshot => {
        if (!snapshot.exists) {
          document.set(user);
        } else {
          throw ({ exists: true });
        }
      }));
  }

  delete(email: string) {
    return this.firestore
      .collection('users')
      .doc(email)
      .delete();
  }

}
