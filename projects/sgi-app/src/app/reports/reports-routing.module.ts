import { CoordinatorGuard } from './../core/guards/coordinator.guard';
import { ReportsGeneralComponent } from './reports-general/reports-general.component';
import { ReportsHomeComponent } from './reports-home/reports-home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: ReportsHomeComponent,
    canActivate: [CoordinatorGuard],
  },
  {
    path: 'general',
    component: ReportsGeneralComponent,
    canActivate: [CoordinatorGuard],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
