import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsGeneralComponent } from './reports-general.component';

xdescribe('ReportsGeneralComponent', () => {
  let component: ReportsGeneralComponent;
  let fixture: ComponentFixture<ReportsGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
