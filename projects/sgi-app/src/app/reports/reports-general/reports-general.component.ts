import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { CatalogService } from '../../incidents/incidents-shared/services/catalog/catalog.service';
import { ResportsGeneralService } from './resports-general.service';

@Component({
  selector: 'app-reports-general',
  templateUrl: './reports-general.component.html',
  styleUrls: ['./reports-general.component.scss']
})
export class ReportsGeneralComponent implements OnInit {

  constructor(
    private catalogService: CatalogService,
    private reportService: ResportsGeneralService
  ) { }

  form = new FormGroup({
    citizen: new FormControl(''),
    type: new FormControl(''),
    area: new FormControl(''),
    reason: new FormControl(''),
    problem: new FormControl(''),
    incidence: new FormControl(''),
    state: new FormControl(''),
    start: new FormControl(''),
    end: new FormControl('')
  });
  area$ = this.catalogService.areas();
  incidents = [];
  typifications: any;
  reasons: [];
  problems: [];
  incidences: [];
  selected = [];

  ngOnInit(): void {
    this.catalogService.typifications().subscribe(
      typifications => {
        this.typifications = typifications;
        this.reasons = this.typifications.reasons;
      });
  }

  onReasonChange() {
    this.problems = this.typifications.problems.filter(item => item.reason === this.form.value.reason);
    this.form.controls.problem.setValue('');
    this.incidences = [];
    this.form.controls.incidence.setValue('');
  }

  onProblemChange() {
    this.incidences = this.typifications.incidences.filter(item => item.problem === this.form.value.problem);
    this.form.controls.incidence.setValue('');
  }

  export() {
    this.reportService.getData(this.form.value).then(data => {

      const formatted = data.map((item: any) => {
        return {
          code: item.code,
          dni: item.dni,
          email: item.email,
          phone: item.phone,
          district: item.district,
          address: item.address,
          registerDate: item.registerDate.toDate(),
          description: item.description,
          importance: item.importance,
          type: item.type,
          response: item.responce
        }
      });

      const headers = {
        code: 'Código',
        dni: 'DNI',
        email: 'Correo',
        phone: 'Teléfono',
        distrit: 'Distrito',
        address: 'Dirección',
        registerDate: 'Fecha Registro',
        description: 'Incidencia',
        importance: 'Importancia',
        type: 'Tipo',
        response: 'Respuesta'
      };

      this.exportCSVFile(headers, formatted, 'incidencias');
    });
  }

  clear() {
    this.form.reset();
  }

  convertToCSV(objArray) {
    const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < array.length; i++) {
      let line = '';
      // tslint:disable-next-line: forin
      for (let index in array[i]) {
        if (line !== '') {
          line += ','
        };
        line += array[i][index];
      }
      str += line + '\r\n';
    }
    return str;
  }

  exportCSVFile(headers, items, fileName) {
    if (headers) {
      items.unshift(headers);
    }
    const jsonObject = JSON.stringify(items);
    const csv = this.convertToCSV(jsonObject);
    const exportName = fileName + '.csv' || 'export.csv';
    const blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) {
      navigator.msSaveBlob(blob, exportName);
    } else {
      const link = document.createElement('a');
      if (link.download !== undefined) {
        const url = URL.createObjectURL(blob);
        link.setAttribute('href', url);
        link.setAttribute('download', exportName);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }
}
