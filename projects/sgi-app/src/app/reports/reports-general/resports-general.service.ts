import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Collection } from '../../core/models/collection.enum';

@Injectable({
  providedIn: 'root'
})
export class ResportsGeneralService {

  constructor(
    private firestore: AngularFirestore
  ) { }

  async getData(filters: any) {
    let collection = this.firestore.firestore.collection(Collection.Incidents);

    collection = this.getWhereReferences(collection, filters);

    return collection.get().then(snapshot => {
      const list = snapshot.docs.map(doc => {
        const data = doc.data();
        const id = doc.id;
        return { id, ...data };
      });
      return list;
    });
  }

  getWhereReferences(collection, filters) {
    if (filters.citizen) {
      collection = collection.where('dni', '==', filters.citizen);
    }

    if (filters.type) {
      collection = collection.where('type', '==', filters.type);
    }

    if (filters.area) {
      collection = collection.where('areaId', '==', filters.area);
    }

    if (filters.reason) {
      collection = collection.where('reasonId', '==', filters.reason);
    }

    if (filters.problem) {
      collection = collection.where('problemId', '==', filters.problem);
    }

    if (filters.incidence) {
      collection = collection.where('incidenceId', '==', filters.incidence);
    }

    if (filters.state) {
      collection = collection.where('status', '==', filters.state);
    }

    return collection;
  }
}
