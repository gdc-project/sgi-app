import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsGeneralComponent } from './reports-general/reports-general.component';
import { ReportsHomeComponent } from './reports-home/reports-home.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


@NgModule({
  declarations: [ReportsGeneralComponent, ReportsHomeComponent],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    CoreModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
  ]
})
export class ReportsModule { }
