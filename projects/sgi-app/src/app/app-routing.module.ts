import { AuthGuard } from './core/guards/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TemplateComponent } from './core/template/template.component';
import { SignInComponent } from './security/sign-in/sign-in.component';


const routes: Routes = [
  {
    path: 'ingresar',
    component: SignInComponent
  },
  {
    path: '',
    canActivate: [AuthGuard],
    component: TemplateComponent,
    children: [
      {
        path: '',
        canActivate: [AuthGuard],
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'incidencias',
        canActivate: [AuthGuard],
        loadChildren: () => import('./incidents/incidents.module').then(m => m.IncidentsModule)
      },
      {
        path: 'reportes',
        canActivate: [AuthGuard],
        loadChildren: () => import('./reports/reports.module').then(m => m.ReportsModule)
      },
      {
        path: 'seguridad',
        canActivate: [AuthGuard],
        loadChildren: () => import('./security/security.module').then(m => m.SecurityModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
