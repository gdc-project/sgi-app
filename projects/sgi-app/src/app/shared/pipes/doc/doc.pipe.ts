import { Pipe, PipeTransform } from '@angular/core';
import { FirestoreService } from '../../../core/firestore/firestore.service';
import { Observable, EMPTY } from 'rxjs';

@Pipe({
  name: 'doc'
})
export class DocPipe implements PipeTransform {

  constructor(private firestore: FirestoreService) { }

  transform(value: any): Observable<any> {
    if (!value) {
      return EMPTY;
    }
    return this.firestore.docWithId$(value.path);
  }

}
