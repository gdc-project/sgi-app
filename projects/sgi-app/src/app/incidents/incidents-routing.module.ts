import { CoordinatorGuard } from './../core/guards/coordinator.guard';
import { ManagerGuard } from './../core/guards/manager.guard';
import { IncidentsNotifyComponent } from './incidents-notify/incidents-notify.component';
import { IncidentsEndComponent } from './incidents-end/incidents-end.component';
import { IncidentsWorkComponent } from './incidents-work/incidents-work.component';
import { IncidentsAssignComponent } from './incidents-assign/incidents-assign.component';
import { IncidentsRegisterComponent } from './incidents-register/incidents-register.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminGuard } from '../core/guards/admin.guard';


const routes: Routes = [
  {
    path: 'registrar',
    component: IncidentsRegisterComponent,
    canActivate: [ManagerGuard],
  },
  {
    path: 'asignar',
    component: IncidentsAssignComponent,
    canActivate: [CoordinatorGuard],
  },
  {
    path: 'trabajar',
    component: IncidentsWorkComponent,
    canActivate: [ManagerGuard],
  },
  {
    path: 'finalizar',
    component: IncidentsEndComponent,
    canActivate: [AdminGuard],
  },
  {
    path: 'notificar',
    component: IncidentsNotifyComponent,
    canActivate: [ManagerGuard],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncidentsRoutingModule { }
