import { IncidentsNotifyService } from './../../incidents/incidents-notify/incidents-notify.service';
import { Component, NgZone, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { debounceTime } from 'rxjs/operators';
import { CatalogService } from '../incidents-shared/services/catalog/catalog.service';

@Component({
  selector: 'app-incidents-notify',
  templateUrl: './incidents-notify.component.html',
  styleUrls: ['./incidents-notify.component.scss']
})
export class IncidentsNotifyComponent implements OnInit {

  modalRef: BsModalRef;
  area$ = this.catalogService.areas();
  incidents = [];
  form = new FormGroup({
    citizen: new FormControl(''),
    type: new FormControl(''),
    area: new FormControl(''),
    reason: new FormControl(''),
    problem: new FormControl(''),
    incidence: new FormControl(''),
  });
  typifications: any;
  reasons: [];
  problems: [];
  incidences: [];
  selected = [];

  constructor(
    private catalogService: CatalogService,
    private incidentsService: IncidentsNotifyService,
    private ngZone: NgZone,
    private modalService: BsModalService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.incidentsService.loadAttendedIncidents();
    this.incidentsService.incident$
      .pipe(debounceTime(100))
      .subscribe(incidents => {
        this.ngZone.run(() => this.incidents = incidents);
      });

    this.catalogService.typifications().subscribe(
      typifications => {
        this.typifications = typifications;
        this.reasons = this.typifications.reasons;
      });
  }

  onReasonChange() {
    this.problems = this.typifications.problems.filter(item => item.reason === this.form.value.reason);
    this.form.controls.problem.setValue('');
    this.incidences = [];
    this.form.controls.incidence.setValue('');
  }

  onProblemChange() {
    this.incidences = this.typifications.incidences.filter(item => item.problem === this.form.value.problem);
    this.form.controls.incidence.setValue('');
  }

  search() {
    this.incidentsService.filterAttendedIncidents(this.form.value);
  }

  openDetails(template: TemplateRef<any>, incident) {
    this.incidentsService.incident = incident;
    this.modalRef = this.modalService.show(template, { class: 'modal-full' });
  }

  openNotifyConfirm(template: TemplateRef<any>, incident) {
    this.incidentsService.incident = incident;
    this.modalRef = this.modalService.show(template);
  }

  return() {
    this.incidentsService.notify().then(
      () => {
        this.modalRef.hide();
        this.toastr.success('La incidencia fue notificada');
      }
    );
  }

}
