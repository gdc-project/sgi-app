import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentsNotifyComponent } from './incidents-notify.component';

xdescribe('IncidentsNotifyComponent', () => {
  let component: IncidentsNotifyComponent;
  let fixture: ComponentFixture<IncidentsNotifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentsNotifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentsNotifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
