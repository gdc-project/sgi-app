import { Injectable } from '@angular/core';
import { FirestoreService } from 'projects/sgi-app/src/app/core/firestore/firestore.service';
import { User } from 'projects/sgi-app/src/app/core/models/user.model';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private firestore: FirestoreService) { }

  managers() {
    return this.firestore.col$<User>('users',
      ref => ref.where('roles.manager', '==', true).where('isActive', '==', true));
  }
}
