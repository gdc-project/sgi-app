import { District } from '../../../../core/models/disctric.model';
import { Area } from '../../../incidents-models/area.model';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { FirestoreService } from '../../../../core/firestore/firestore.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  constructor(private firestore: FirestoreService) { }

  areas(): Observable<Area[]> {
    return this.firestore.col$<Area>('areas');
  }

  types() {
    return [
      { id: 'Orientación', name: 'Orientación' },
      { id: 'Reclamo', name: 'Reclamo' },
      { id: 'Sugerencia', name: 'Sugerencia' },
    ];
  }

  districts() {
    return this.firestore.col$<District>('districts').pipe(
      map(res => res.sort((a, b) => {
        const name1 = a.name.toUpperCase();
        const name2 = b.name.toUpperCase();
        return (name1 < name2) ? -1 : (name1 > name2) ? 1 : 0;
      }))
    );
  }
  typifications(): Observable<any> {
    return this.firestore.col$('typifications').pipe(
      map(items => {

        const incidences = items.map((item: any) => {
          return { id: item.incidenceId, name: item.incidence, problem: item.problemId };
        });

        const problems = [...new Set(items.map((item: any) => {
          return { id: item.problemId, name: item.problem, reason: item.reasonId };
        }))].filter((thing, index, self) =>
          index === self.findIndex((t) => (
            t.id === thing.id && t.name === thing.name
          ))
        );

        const reasons = [...new Set(items.map((item: any) => {
          return { id: item.reasonId, name: item.reason };
        }))].filter((thing, index, self) =>
          index === self.findIndex((t) => (
            t.id === thing.id && t.name === thing.name
          ))
        );
        return { reasons, problems, incidences };
      })
    );
  }
}
