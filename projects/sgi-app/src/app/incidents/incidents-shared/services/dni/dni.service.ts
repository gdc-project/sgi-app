import { Injectable } from '@angular/core';
import { environment } from 'projects/sgi-app/src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DniService {

  API_RENIEC = environment.apipe.url;
  API_TOKEN = environment.apipe.token;

  constructor(private http: HttpClient) { }

  getDataUser(dni: string) {
    return this.http.get(`${this.API_RENIEC}/dni/${dni}?token=${this.API_TOKEN}`);
  }
}
