import { TestBed } from '@angular/core/testing';

import { ElapsedService } from './elapsed.service';

describe('ElapsedService', () => {
  let service: ElapsedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ElapsedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
