import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class ElapsedService {

  constructor() { }

  days(date: Date) {
    const past = moment(date).startOf('day');
    const now = moment().startOf('day');

    let elapsed = 0;
    for (const current = moment(past); current.isBefore(now); current.add(1, 'days')) {
      const weekday = current.weekday();
      if (weekday !== 6 && weekday !== 0) {
        elapsed++;
      }
    }

    return elapsed;
  }
}
