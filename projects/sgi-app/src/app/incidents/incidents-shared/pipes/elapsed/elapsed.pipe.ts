import { Pipe, PipeTransform } from '@angular/core';
import { ElapsedService } from '../../services/elapsed/elapsed.service';

@Pipe({
  name: 'elapsed'
})
export class ElapsedPipe implements PipeTransform {

  constructor(private elapsed: ElapsedService) { }

  transform(date: Date): number {
    return this.elapsed.days(date);
  }

}
