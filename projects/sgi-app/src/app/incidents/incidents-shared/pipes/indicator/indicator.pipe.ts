import { Pipe, PipeTransform } from '@angular/core';
import { ElapsedService } from '../../services/elapsed/elapsed.service';

@Pipe({
  name: 'indicator'
})
export class IndicatorPipe implements PipeTransform {

  constructor(private elapsed: ElapsedService) { }

  transform(date: Date): string {
    const elapsed = this.elapsed.days(date);

    if (elapsed < 15) {
      return 'success';
    }

    if (elapsed >= 15 && elapsed < 30) {
      return 'warning';
    }

    if (elapsed >= 30) {
      return 'danger';
    }
  }

}
