import { Component, OnInit } from '@angular/core';
import { Incident } from '../../incidents-models/incident.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { IncidentsEndService } from '../incidents-end.service';

@Component({
  selector: 'app-incidents-review',
  templateUrl: './incidents-review.component.html',
  styleUrls: ['./incidents-review.component.scss']
})
export class IncidentsReviewComponent implements OnInit {


  incident: Incident;
  form = new FormGroup({
    response: new FormControl('', Validators.required),
  });

  constructor(
    private modalService: BsModalService,
    private incidentsService: IncidentsEndService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.incident = this.incidentsService.incident;
    this.form.controls.response.setValue(this.incident.response);
  }

  saveAndComplete() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      const response = this.form.value.response;

      this.incidentsService.finish(this.incident.id, response).then(
        () => {
          this.modalService.hide(1);
          this.toastr.success('La incidencia fue completada');
        }
      );
    }
  }

  isValidControl(controlName: string) {
    return this.form.controls[controlName].valid || this.form.controls[controlName].untouched;
  }
}
