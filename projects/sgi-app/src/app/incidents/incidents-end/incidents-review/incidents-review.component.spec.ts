import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentsReviewComponent } from './incidents-review.component';

xdescribe('IncidentsReviewComponent', () => {
  let component: IncidentsReviewComponent;
  let fixture: ComponentFixture<IncidentsReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentsReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentsReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
