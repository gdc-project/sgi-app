import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentsEndComponent } from './incidents-end.component';

xdescribe('IncidentsEndComponent', () => {
  let component: IncidentsEndComponent;
  let fixture: ComponentFixture<IncidentsEndComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentsEndComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentsEndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
