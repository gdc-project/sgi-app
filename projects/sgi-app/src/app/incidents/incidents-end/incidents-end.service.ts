import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../../core/security/auth.service';
import { Incident } from '../incidents-models/incident.model';
import { Subject } from 'rxjs';
import { Collection } from '../../core/models/collection.enum';
import { State } from '../incidents-models/status.enum';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class IncidentsEndService {

  incident: Incident;
  incident$ = new Subject<Incident[]>();

  constructor(
    private firestore: AngularFirestore,
    private auth: AuthService
  ) { }

  loadAttendedIncidents() {
    this.firestore.firestore.collection(Collection.Incidents)
      .where('status', '==', State.Attended)
      .where('assignedBy', '==', this.auth.user.email)
      .onSnapshot(snapshot => {
        const list = snapshot.docs.map(doc => {
          const data = doc.data();
          const id = doc.id;
          return { id, ...data };
        });
        this.incident$.next(list as Incident[]);
      });
  }

  filterAttendedIncidents(filters: any) {
    let collection = this.firestore.firestore.collection(Collection.Incidents)
      .where('status', '==', State.Attended)
      .where('assignedBy', '==', this.auth.user.email);

    collection = this.getWhereReferences(collection, filters);

    collection.onSnapshot(snapshot => {
      const list = snapshot.docs.map(doc => {
        const data = doc.data();
        const id = doc.id;
        return { id, ...data };
      });
      this.incident$.next(list as Incident[]);
    });
  }

  getWhereReferences(collection, filters) {
    if (filters.citizen) {
      collection = collection.where('dni', '==', filters.citizen);
    }

    if (filters.type) {
      collection = collection.where('type', '==', filters.type);
    }

    if (filters.area) {
      collection = collection.where('areaId', '==', filters.area);
    }

    if (filters.reason) {
      collection = collection.where('reasonId', '==', filters.reason);
    }

    if (filters.problem) {
      collection = collection.where('problemId', '==', filters.problem);
    }

    if (filters.incidence) {
      collection = collection.where('incidenceId', '==', filters.incidence);
    }

    return collection;
  }

  return() {
    return this.firestore
      .collection(Collection.Incidents)
      .doc(this.incident.id)
      .set({ status: State.Assigned }, { merge: true });
  }

  finish(id, response: string) {
    return this.firestore
      .collection(Collection.Incidents)
      .doc(id)
      .set({
        response,
        status: State.Finished,
        workedUpDate: firebase.firestore.FieldValue.serverTimestamp(),
      }, { merge: true });
  }
}
