import { IncidentsAssignService } from './../../incidents-assign/incidents-assign.service';
import { Observable } from 'rxjs';
import { Area } from './../../incidents-models/area.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Incident } from './../../incidents-models/incident.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CatalogService } from '../../incidents-shared/services/catalog/catalog.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-incidents-edit',
  templateUrl: './incidents-edit.component.html',
  styleUrls: ['./incidents-edit.component.scss']
})
export class IncidentsEditComponent implements OnInit {

  incident: Incident;
  area$: Observable<Area[]>;
  types: any[];
  typifications: any;
  reasons: [];
  problems: [];
  incidences: [];
  form = new FormGroup({
    type: new FormControl('', Validators.required),
    area: new FormControl('', Validators.required),
    reason: new FormControl('', Validators.required),
    problem: new FormControl('', Validators.required),
    incidence: new FormControl('', Validators.required),
  });

  constructor(
    private incidentsService: IncidentsAssignService,
    private catalogService: CatalogService,
    private modalService: BsModalService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.incident = this.incidentsService.incident;
    this.area$ = this.catalogService.areas();
    this.types = this.catalogService.types();

    this.catalogService.typifications().subscribe(typifications => {
      this.typifications = typifications;
      this.reasons = this.typifications.reasons;
      this.loadValues();
    });
  }

  loadValues() {
    this.form.patchValue({
      type: this.incident.type,
      area: this.incident.areaId
    });

    this.form.controls.reason.setValue(this.incident.reasonId);
    this.onReasonChange();
    this.form.controls.problem.setValue(this.incident.problemId);
    this.onProblemChange();
    this.form.controls.incidence.setValue(this.incident.incidenceId);
  }

  onReasonChange() {
    this.problems = this.typifications.problems.filter(item => item.reason === this.form.value.reason);
    this.form.controls.problem.setValue('');
    this.incidences = [];
    this.form.controls.incidence.setValue('');
  }

  onProblemChange() {
    this.incidences = this.typifications.incidences.filter(item => item.problem === this.form.value.problem);
    this.form.controls.incidence.setValue('');
  }

  isValidControl(controlName: string) {
    return this.form.controls[controlName].valid || this.form.controls[controlName].untouched;
  }

  save() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.incidentsService.updateIncident(this.incident.id, this.form.value).then(
        () => {
          this.modalService.hide(1);
          this.toastr.success('La incidencia fue editada');
        });
    }
  }
}
