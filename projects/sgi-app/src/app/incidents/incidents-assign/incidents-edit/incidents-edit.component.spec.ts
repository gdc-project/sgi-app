import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentsEditComponent } from './incidents-edit.component';

xdescribe('IncidentsEditComponent', () => {
  let component: IncidentsEditComponent;
  let fixture: ComponentFixture<IncidentsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
