import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentsDistributeComponent } from './incidents-distribute.component';

xdescribe('IncidentsDistributeComponent', () => {
  let component: IncidentsDistributeComponent;
  let fixture: ComponentFixture<IncidentsDistributeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentsDistributeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentsDistributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
