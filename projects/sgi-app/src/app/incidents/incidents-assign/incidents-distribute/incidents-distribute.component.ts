import { IncidentsAssignService } from './../../incidents-assign/incidents-assign.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { UserService } from '../../incidents-shared/services/user/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-incidents-distribute',
  templateUrl: './incidents-distribute.component.html',
  styleUrls: ['./incidents-distribute.component.scss']
})
export class IncidentsDistributeComponent implements OnInit {

  manager$ = this.userService.managers();
  importances = [
    { name: 'Baja' },
    { name: 'Media' },
    { name: 'Alta' },
  ];
  form = new FormGroup({
    manager: new FormControl('', Validators.required),
    importance: new FormControl('', Validators.required),
  });

  constructor(
    private userService: UserService,
    private incidentService: IncidentsAssignService,
    private modalService: BsModalService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
  }

  distribute() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.incidentService.distributeIncident(this.incidentService.selectedIncidents, this.form.value).then(
        () => {
          this.modalService.hide(1);
          this.toastr.success('Las incidencias fueron asignadas');
        }
      );
    }
  }
}
