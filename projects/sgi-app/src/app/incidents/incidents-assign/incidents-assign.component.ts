import { IncidentsAssignService } from './incidents-assign.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { CatalogService } from '../incidents-shared/services/catalog/catalog.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, TemplateRef, NgZone } from '@angular/core';
import { debounceTime } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-incidents-assign',
  templateUrl: './incidents-assign.component.html',
  styleUrls: ['./incidents-assign.component.scss']
})
export class IncidentsAssignComponent implements OnInit {

  modalRef: BsModalRef;
  area$ = this.catalogService.areas();
  incidents = [];
  form = new FormGroup({
    citizen: new FormControl(''),
    type: new FormControl(''),
    area: new FormControl(''),
  });
  selected = [];

  constructor(
    private ngZone: NgZone,
    private incidentsService: IncidentsAssignService,
    private catalogService: CatalogService,
    private modalService: BsModalService,
    private toastr: ToastrService
    ) { }

  ngOnInit(): void {
    this.incidentsService.loadNewIncidents();
    this.incidentsService.incident$
      .pipe(debounceTime(100))
      .subscribe(incidents => {
        this.ngZone.run(() => this.incidents = incidents);
      });
  }

  openEdit(template: TemplateRef<any>, incident) {
    this.incidentsService.incident = incident;
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }

  openAssign(template: TemplateRef<any>) {
    this.incidentsService.selectedIncidents = this.selected;
    this.modalRef = this.modalService.show(template);
  }

  openOverride(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  search() {
    this.incidentsService.filterNewIncidents(this.form.value);
  }

  toggleSelected(incident): void {
    const index = this.selected.findIndex(item => item.id === incident.id);

    if (index < 0) {
      this.selected.push(incident);
    } else {
      this.selected.splice(index, 1);
    }
  }

  override() {
    this.incidentsService.overrideIncident(this.selected).then(
      () => {
        this.modalRef.hide();
        this.toastr.success('Las incidencias fueron anuladas');
        this.selected = [];
      }
    );
  }
}
