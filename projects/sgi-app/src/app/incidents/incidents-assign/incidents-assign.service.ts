import { Collection } from './../../core/models/collection.enum';
import { AuthService } from './../../core/security/auth.service';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { State } from '../incidents-models/status.enum';
import { Incident } from '../incidents-models/incident.model';
import { Subject } from 'rxjs';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class IncidentsAssignService {

  selectedIncidents = [];
  incident: Incident;
  incident$ = new Subject<Incident[]>();

  constructor(
    private firestore: AngularFirestore,
    private auth: AuthService
  ) { }

  loadNewIncidents() {
    this.firestore.firestore.collection(Collection.Incidents)
      .where('status', '==', State.New)
      .onSnapshot(snapshot => {
        const list = snapshot.docs.map(doc => {
          const data = doc.data();
          const id = doc.id;
          return { id, ...data };
        });
        this.incident$.next(list as Incident[]);
      });
  }

  filterNewIncidents(filters: any) {
    let collection = this.firestore.firestore.collection(Collection.Incidents)
      .where('status', '==', State.New);

    collection = this.getWhereReferences(collection, filters);

    collection.onSnapshot(snapshot => {
      const list = snapshot.docs.map(doc => {
        const data = doc.data();
        const id = doc.id;
        return { id, ...data };
      });
      this.incident$.next(list as Incident[]);
    });
  }

  getWhereReferences(collection, filters) {
    if (filters.citizen) {
      collection = collection.where('dni', '==', filters.citizen);
    }

    if (filters.type) {
      collection = collection.where('type', '==', filters.type);
    }

    if (filters.area) {
      collection = collection.where('areaId', '==', filters.area);
    }

    return collection;
  }

  updateIncident(id: string, values: any) {
    const request = {
      type: values.type,
      area: this.firestore.collection(Collection.Areas).doc(values.area).ref,
      areaId: values.area,
      typification: this.firestore.collection(Collection.Typifications).doc(values.incidence).ref,
      reasonId: values.reason,
      problemId: values.problem,
      incidenceId: values.incidence,
    };

    return this.firestore
      .collection(Collection.Incidents)
      .doc(id)
      .set(request, { merge: true });
  }

  distributeIncident(incidents: Incident[], values: any) {
    const request = {
      assignedBy: this.auth.user.email,
      assignedTo: values.manager,
      importance: values.importance,
      assignedDate: firebase.firestore.FieldValue.serverTimestamp(),
      status: State.Assigned
    };

    const list = [];
    incidents.forEach(element => {
      list.push(this.firestore
        .collection(Collection.Incidents)
        .doc(element.id)
        .set(request, { merge: true }));
    });

    return Promise.all(list);
  }

  overrideIncident(incidents: Incident[]) {
    const request = {
      canceledBy: this.auth.user.email,
      status: State.Canceled
    };

    const list = [];
    incidents.forEach(element => {
      list.push(this.firestore
        .collection(Collection.Incidents)
        .doc(element.id)
        .set(request, { merge: true }));
    });

    return Promise.all(list);
  }
}
