import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentsAssignComponent } from './incidents-assign.component';

xdescribe('IncidentsAssignComponent', () => {
  let component: IncidentsAssignComponent;
  let fixture: ComponentFixture<IncidentsAssignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentsAssignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentsAssignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
