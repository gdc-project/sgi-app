import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentsWorkComponent } from './incidents-work.component';

xdescribe('IncidentsWorkComponent', () => {
  let component: IncidentsWorkComponent;
  let fixture: ComponentFixture<IncidentsWorkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentsWorkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentsWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
