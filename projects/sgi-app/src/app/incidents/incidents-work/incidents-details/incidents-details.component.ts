import { Component, OnInit } from '@angular/core';
import { Incident } from '../../incidents-models/incident.model';
import { BsModalService } from 'ngx-bootstrap/modal';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IncidentsWorkService } from '../incidents-work.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-incidents-details',
  templateUrl: './incidents-details.component.html',
  styleUrls: ['./incidents-details.component.scss']
})
export class IncidentsDetailsComponent implements OnInit {

  incident: Incident;
  form = new FormGroup({
    response: new FormControl('', Validators.required),
  });

  constructor(
    private modalService: BsModalService,
    private incidentsService: IncidentsWorkService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.incident = this.incidentsService.incident;
    this.form.controls.response.setValue(this.incident.response);
  }

  save() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      const response = this.form.value.response;

      this.incidentsService.saveResponse(this.incident.id, response).then(
        () => {
          this.form.markAsPristine();
          this.toastr.success('La incidencia fue guardada');
        }
      );
    }
  }

  saveAndComplete() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      const response = this.form.value.response;

      this.incidentsService.completeResponse(this.incident.id, response).then(
        () => {
          this.modalService.hide(1);
          this.toastr.success('La incidencia fue completada');
        }
      );
    }
  }

  isValidControl(controlName: string) {
    return this.form.controls[controlName].valid || this.form.controls[controlName].untouched;
  }
}
