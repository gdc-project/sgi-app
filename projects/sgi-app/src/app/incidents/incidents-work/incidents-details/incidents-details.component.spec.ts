import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentsDetailsComponent } from './incidents-details.component';

xdescribe('IncidentsDetailsComponent', () => {
  let component: IncidentsDetailsComponent;
  let fixture: ComponentFixture<IncidentsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
