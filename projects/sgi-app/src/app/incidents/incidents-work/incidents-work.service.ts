import { AuthService } from './../../core/security/auth.service';
import { Injectable } from '@angular/core';
import { Collection } from '../../core/models/collection.enum';
import { State } from '../incidents-models/status.enum';
import { Incident } from '../incidents-models/incident.model';
import { Subject } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class IncidentsWorkService {

  incident: Incident;
  incident$ = new Subject<Incident[]>();

  constructor(
    private firestore: AngularFirestore,
    private auth: AuthService
  ) { }

  loadAssignedIncidents() {
    this.firestore.firestore.collection(Collection.Incidents)
      .where('status', '==', State.Assigned)
      .where('assignedTo', '==', this.auth.user.email)
      .onSnapshot(snapshot => {
        const list = snapshot.docs.map(doc => {
          const data = doc.data();
          const id = doc.id;
          return { id, ...data };
        });
        this.incident$.next(list as Incident[]);
      });
  }

  filterNewIncidents(filters: any) {
    let collection = this.firestore.firestore.collection(Collection.Incidents)
      .where('status', '==', State.Assigned)
      .where('assignedTo', '==', this.auth.user.email);

    collection = this.getWhereReferences(collection, filters);

    collection.onSnapshot(snapshot => {
      const list = snapshot.docs.map(doc => {
        const data = doc.data();
        const id = doc.id;
        return { id, ...data };
      });
      this.incident$.next(list as Incident[]);
    });
  }

  getWhereReferences(collection, filters) {
    if (filters.citizen) {
      collection = collection.where('dni', '==', filters.citizen);
    }

    if (filters.type) {
      collection = collection.where('type', '==', filters.type);
    }

    if (filters.area) {
      collection = collection.where('areaId', '==', filters.area);
    }

    if (filters.reason) {
      collection = collection.where('reasonId', '==', filters.reason);
    }

    if (filters.problem) {
      collection = collection.where('problemId', '==', filters.problem);
    }

    if (filters.incidence) {
      collection = collection.where('incidenceId', '==', filters.incidence);
    }

    return collection;
  }

  liberate() {
    return this.firestore
      .collection(Collection.Incidents)
      .doc(this.incident.id)
      .set({ status: State.New }, { merge: true });
  }

  saveResponse(id: string, response: string) {
    return this.firestore
      .collection(Collection.Incidents)
      .doc(id)
      .set({ response }, { merge: true });
  }


  completeResponse(id, response: string) {
    return this.firestore
      .collection(Collection.Incidents)
      .doc(id)
      .set({
        response,
        status: State.Attended,
        workedUpDate: firebase.firestore.FieldValue.serverTimestamp(),
      }, { merge: true });
  }
}
