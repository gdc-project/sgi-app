import { Component, OnInit, TemplateRef, NgZone } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl } from '@angular/forms';
import { CatalogService } from '../incidents-shared/services/catalog/catalog.service';
import { IncidentsWorkService } from './incidents-work.service';
import { debounceTime } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-incidents-work',
  templateUrl: './incidents-work.component.html',
  styleUrls: ['./incidents-work.component.scss']
})
export class IncidentsWorkComponent implements OnInit {

  modalRef: BsModalRef;
  area$ = this.catalogService.areas();
  incidents = [];
  form = new FormGroup({
    citizen: new FormControl(''),
    type: new FormControl(''),
    area: new FormControl(''),
    reason: new FormControl(''),
    problem: new FormControl(''),
    incidence: new FormControl(''),
  });
  typifications: any;
  reasons: [];
  problems: [];
  incidences: [];
  selected = [];

  constructor(
    private incidentsService: IncidentsWorkService,
    private catalogService: CatalogService,
    private modalService: BsModalService,
    private ngZone: NgZone,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.incidentsService.loadAssignedIncidents();
    this.incidentsService.incident$
      .pipe(debounceTime(100))
      .subscribe(incidents => {
        this.ngZone.run(() => this.incidents = incidents);
      });

    this.catalogService.typifications().subscribe(
      typifications => {
        this.typifications = typifications;
        this.reasons = this.typifications.reasons;
      });
  }

  onReasonChange() {
    this.problems = this.typifications.problems.filter(item => item.reason === this.form.value.reason);
    this.form.controls.problem.setValue('');
    this.incidences = [];
    this.form.controls.incidence.setValue('');
  }

  onProblemChange() {
    this.incidences = this.typifications.incidences.filter(item => item.problem === this.form.value.problem);
    this.form.controls.incidence.setValue('');
  }

  search() {
    this.incidentsService.filterNewIncidents(this.form.value);
  }

  openDetails(template: TemplateRef<any>, incident) {
    this.incidentsService.incident = incident;
    this.modalRef = this.modalService.show(template, { class: 'modal-full' });
  }

  openLiberateConfirm(template: TemplateRef<any>, incident) {
    this.incidentsService.incident = incident;
    this.modalRef = this.modalService.show(template);
  }

  liberate() {
    this.incidentsService.liberate().then(
      () => {
        this.modalRef.hide();
        this.toastr.success('La incidencia fue liberada');
      }
    );
  }
}
