import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from './../core/core.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IncidentsRoutingModule } from './incidents-routing.module';
import { IncidentsRegisterComponent } from './incidents-register/incidents-register.component';
import { IncidentsWorkComponent } from './incidents-work/incidents-work.component';
import { IncidentsEndComponent } from './incidents-end/incidents-end.component';
import { IncidentsNotifyComponent } from './incidents-notify/incidents-notify.component';
import { IncidentsAssignComponent } from './incidents-assign/incidents-assign.component';
import { SharedModule } from '../shared/shared.module';
import { ElapsedPipe } from './incidents-shared/pipes/elapsed/elapsed.pipe';
import { IndicatorPipe } from './incidents-shared/pipes/indicator/indicator.pipe';
import { IncidentsDetailsComponent } from './incidents-work/incidents-details/incidents-details.component';
import { IncidentsEditComponent } from './incidents-assign/incidents-edit/incidents-edit.component';
import { IncidentsDistributeComponent } from './incidents-assign/incidents-distribute/incidents-distribute.component';
import { IncidentsReviewComponent } from './incidents-end/incidents-review/incidents-review.component';

@NgModule({
  declarations: [
    IncidentsRegisterComponent,
    IncidentsWorkComponent,
    IncidentsEndComponent,
    IncidentsNotifyComponent,
    IncidentsAssignComponent,
    ElapsedPipe,
    IndicatorPipe,
    IncidentsEditComponent,
    IncidentsDistributeComponent,
    IncidentsDetailsComponent,
    IncidentsReviewComponent
  ],
  imports: [
    CommonModule,
    IncidentsRoutingModule,
    CoreModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule
  ]
})
export class IncidentsModule { }
