export interface Citizen {
    dni: string;
    names: string;
    surnames: string;
}
