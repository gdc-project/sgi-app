import { Subarea } from './subarea.model';

export interface Area {
    name: string;
    description: boolean;
    subareas?: Subarea[];
}
