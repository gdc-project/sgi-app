
export interface Typification {
    reason: string;
    reasonId: number;
}
