export interface Incident {
  area: any;
  areaId: string;
  address: string;
  channer: string;
  citizen: any;
  channel: string;
  code: string;
  description: string;
  district: string;
  dni: string;
  id: string;
  problemId: number;
  reasonId: number;
  email: string;
  phone: string;
  problem?: number;
  registerDate: any;
  status: string;
  subarea?: number;
  type: string;
  typification: any;
  incidenceId: string;
  response: string;
}
