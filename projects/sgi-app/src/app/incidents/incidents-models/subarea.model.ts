export interface Subarea {
    id: number;
    name: string;
    description: boolean;
}
