export enum State {
  New = 'new',
  Canceled = 'canceled',
  Assigned = 'assigned',
  Attended = 'attended',
  Finished = 'finished',
  Notified = 'notified'
}
