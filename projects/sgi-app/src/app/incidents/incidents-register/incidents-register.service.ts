import { Injectable } from '@angular/core';
import { Reniec } from '../../core/models/reniec.model';
import { Collection } from '../../core/models/collection.enum';
import { Channel } from '../../core/models/channel.enum';
import * as firebase from 'firebase';
import { State } from '../incidents-models/status.enum';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class IncidentsRegisterService {

  constructor(private firestore: AngularFirestore) { }

  create(values: any, reniec: Reniec) {
    return this.firestore
      .collection(Collection.Citizens)
      .doc(values.dni)
      .set({ names: reniec.nombres, surnames: `${reniec.apellidoPaterno} ${reniec.apellidoMaterno}` }, { merge: true })
      .then(() => {

        const request = {
          address: values.address,
          area: this.firestore.collection(Collection.Areas).doc(values.area).ref,
          areaId: values.area,
          channel: Channel.Sgi,
          citizen: this.firestore.collection(Collection.Citizens).doc(values.dni).ref,
          code: Date.now().toPrecision(),
          description: values.description,
          district: values.district,
          dni: values.dni,
          incidenceId: values.incidence,
          problemId: values.problem,
          reasonId: values.reason,
          registerDate: firebase.firestore.FieldValue.serverTimestamp(),
          status: State.New,
          email: values.email,
          phone: values.phone,
          type: values.type,
          typification: this.firestore.collection(Collection.Typifications).doc(values.incidence).ref,
        };

        this.firestore
          .collection(Collection.Incidents)
          .add(request);
      });
  }
}
