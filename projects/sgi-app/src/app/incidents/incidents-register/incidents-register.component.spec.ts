import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentsRegisterComponent } from './incidents-register.component';

xdescribe('IncidentsRegisterComponent', () => {
  let component: IncidentsRegisterComponent;
  let fixture: ComponentFixture<IncidentsRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentsRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentsRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
