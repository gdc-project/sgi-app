import { IncidentsRegisterService } from './incidents-register.service';
import { Reniec } from '../../core/models/reniec.model';
import { CatalogService } from '../incidents-shared/services/catalog/catalog.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DniService } from '../incidents-shared/services/dni/dni.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-incidents-register',
  templateUrl: './incidents-register.component.html',
  styleUrls: ['./incidents-register.component.scss']
})
export class IncidentsRegisterComponent implements OnInit {

  reniec: Reniec;
  isReniecLoading = false;
  district$: any;
  area$: any;
  typifications: any;
  reasons: [];
  problems: [];
  incidences: [];
  form = new FormGroup({
    dni: new FormControl('', Validators.required),
    names: new FormControl({ value: '', disabled: true }, Validators.required),
    surnames: new FormControl({ value: '', disabled: true }, Validators.required),
    email: new FormControl('', Validators.required),
    phone: new FormControl(''),
    district: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    reference: new FormControl(''),
    type: new FormControl('', Validators.required),
    area: new FormControl('', Validators.required),
    reason: new FormControl('', Validators.required),
    problem: new FormControl('', Validators.required),
    incidence: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required)
  });

  constructor(
    private dataApi: DniService,
    private catalogService: CatalogService,
    private incidentService: IncidentsRegisterService,
    private toastr: ToastrService
  ) { }


  ngOnInit(): void {
    this.district$ = this.catalogService.districts();
    this.area$ = this.catalogService.areas();

    this.catalogService.typifications().subscribe(typifications => {
      this.typifications = typifications;
      this.reasons = this.typifications.reasons;
    });
  }

  onSearchChange(dni: string): void {
    if (dni.length === 8) {
      this.getDataUser(dni);
    }
    this.reniec = { nombres: '', apellidoPaterno: '', apellidoMaterno: '' };
    this.form.patchValue({ names: '', surnames: '' });
  }

  getDataUser(dni: string) {
    this.isReniecLoading = true;
    this.dataApi.getDataUser(dni).subscribe(
      (reniec: Reniec) => {
        this.reniec = reniec;
        if (reniec.nombres && reniec.apellidoPaterno) {
          this.form.patchValue({
            names: reniec.nombres,
            surnames: `${reniec.apellidoPaterno} ${reniec.apellidoMaterno}`
          });
          this.form.controls.names.updateValueAndValidity();
          this.form.controls.surnames.updateValueAndValidity();
        }
        this.isReniecLoading = false;
      },
      () => {
        this.isReniecLoading = false;
        this.reniec = { nombres: '', apellidoPaterno: '', apellidoMaterno: '' };
        this.form.patchValue({ dni: '', names: '', surnames: '' });
        this.toastr.error('El documento no fue encontrado');
      }
    );
  }

  onReasonChange() {
    this.problems = this.typifications.problems.filter(item => item.reason === this.form.value.reason);
    this.form.controls.problem.setValue('');
    this.incidences = [];
    this.form.controls.incidence.setValue('');
  }

  onProblemChange() {
    this.incidences = this.typifications.incidences.filter(item => item.problem === this.form.value.problem);
    this.form.controls.incidence.setValue('');
  }

  isReniecValid() {
    return this.reniec?.nombres || this.isValidControl('dni');
  }

  isValidControl(controlName: string) {
    return this.form.controls[controlName].valid || this.form.controls[controlName].untouched;
  }

  save() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.incidentService.create(this.form.value, this.reniec).then(
        () => {
          this.form.reset();
          this.toastr.success('La incidencia fue registrada');
        }
      );
    }
  }
}
