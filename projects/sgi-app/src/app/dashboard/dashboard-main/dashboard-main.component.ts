import { Component, OnInit } from '@angular/core';
import { DashboardMainService } from './dashboard-main.service';
import { IncidentType } from '../../core/models/type.enum';

@Component({
  selector: 'app-dashboard-main',
  templateUrl: './dashboard-main.component.html',
  styleUrls: ['./dashboard-main.component.scss']
})
export class DashboardMainComponent implements OnInit {
  barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  barChartLabels = [];
  barChartType = 'bar';
  barChartLegend = true;
  barChartData = [];

  todayDate = new Date();
  todayAll = 0;
  todayOrientations = 0;
  todayClaims = 0;
  todaySuggestions = 0;

  mainNew = 0;
  mainAssigned = 0;

  constructor(private dashboardService: DashboardMainService) { }

  ngOnInit() {
    this.dashboardService.weekDay$.subscribe(
      days => this.barChartLabels = days
    );

    this.dashboardService.weekIncident$.subscribe(
      data => this.barChartData = data
    );

    this.dashboardService.todayIncident$.subscribe(
      data => {
        this.todayAll = data['All'];
        this.todayOrientations = data[IncidentType.Orientation];
        this.todayClaims = data[IncidentType.Claim];
        this.todaySuggestions = data[IncidentType.Suggestion];
      }
    );

    this.dashboardService.newIncident$.subscribe(
      quantity => this.mainNew = quantity
    );

    this.dashboardService.assignedIncident$.subscribe(
      quantity => this.mainAssigned = quantity
    );

    this.dashboardService.getWeekIncidents();
    this.dashboardService.getTodayIncidents();
    this.dashboardService.getNewIncidents();
    this.dashboardService.getAssignedIncidents();
  }

}
