import { Injectable, Type } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Collection } from '../../core/models/collection.enum';
import * as moment from 'moment';
import { IncidentType } from '../../core/models/type.enum';
import { Subject } from 'rxjs';
import { State } from '../../incidents/incidents-models/status.enum';
import { AuthService } from '../../core/security/auth.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardMainService {

  newIncident$ = new Subject<number>();
  assignedIncident$ = new Subject<number>();
  todayIncident$ = new Subject<any[]>();
  weekIncident$ = new Subject<any[]>();
  weekDay$ = new Subject<any[]>();

  constructor(
    private firestore: AngularFirestore,
    private auth: AuthService
  ) { }

  getWeekIncidents() {

    moment.locale('es');

    const start = moment().add(-7, 'days').startOf('day');
    const end = moment().startOf('day');

    return this.firestore.firestore.collection(Collection.Incidents)
      .where('registerDate', '>', start.toDate())
      .where('registerDate', '<', end.toDate())
      .get().then(
        records => {

          const days = this.getDays(start, end);
          const types = this.getTypes();

          const list = records.docs.map(doc => {
            const data = doc.data();
            const day = moment(data.registerDate.toDate()).format('dddd DD/MM');
            const type = data.type;
            return { type, day };
          });

          const info = [];
          types.forEach(type => {
            const dayData = [];
            days.forEach(day => {
              const quantity = list.filter(item => item.day === day && item.type === type).length;
              dayData.push(quantity);
            });
            info.push({ data: dayData, label: type });
          });

          this.weekIncident$.next(info);
          this.weekDay$.next(days);
        });
  }

  getTodayIncidents() {

    const today = moment().startOf('day');

    return this.firestore.firestore.collection(Collection.Incidents)
      .where('registerDate', '>', today.toDate())
      .onSnapshot(
        snapshot => {

          const types = this.getTypes();

          const list = snapshot.docs.map(doc => {
            const type = doc.data().type;
            return { type };
          });

          const info = [];
          types.forEach(type => {
            info[type] = list.filter(item => item.type === type).length;
          });
          info['All'] = list.length;

          this.todayIncident$.next(info);
        }
      );
  }

  getNewIncidents() {
    return this.firestore.firestore.collection(Collection.Incidents)
      .where('status', '==', State.New)
      .onSnapshot(
        snapshot => {
          const quantity = snapshot.docs.length;
          this.newIncident$.next(quantity);
        }
      );
  }

  getAssignedIncidents() {
    return this.firestore.firestore.collection(Collection.Incidents)
      .where('status', '==', State.Assigned)
      .where('assignedTo', '==', this.auth.user.email)
      .onSnapshot(
        snapshot => {
          const quantity = snapshot.docs.length;
          this.assignedIncident$.next(quantity);
        }
      );
  }

  getTypes() {
    return [IncidentType.Orientation, IncidentType.Claim, IncidentType.Suggestion];
  }

  getDays(start, end) {
    const days = [];

    for (const current = start; start.isBefore(end); current.add(1, 'days')) {
      days.push(current.format('dddd DD/MM'));
    }

    return days;
  }
}
