import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './core/security/auth.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  constructor(public auth: AuthService, private router: Router) {
    this.auth.user$.subscribe(
      response => {
        if (!response) {
          this.router.navigateByUrl('/ingresar');
        }
      }
    );
  }

  ngOnInit() {
    // Load Resize
    $(window).on('load resize', (event) => {
      const windowWidth = $(window).width();
      if (windowWidth < 1010) {
        $('body').addClass('small-device');
      } else {
        $('body').removeClass('small-device');
      }

    });
  }
}
